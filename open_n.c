#include <linux/unistd.h>
#include <linux/linkage.h>
#include <linux/types.h>
#include <linux/syscalls.h>
#include <asm/unistd.h>
#include <asm/uaccess.h>

asmlinkage long sys_open_n(const char **filenames,
                          int flags,
                          umode_t mode,
                          int *buffer,
                          int n,
                          int filenames_length) {
 int i, fd;
 const char *filename;
 
 mm_segment_t fs = get_fs();
 set_fs(KERNEL_DS);
 for (i = 0, filename = (char *)(filenames); i < n; i++, filename += filenames_length) {
     if ((fd = sys_open(filename, flags, mode)) < 0) {
       set_fs(fs);
       return (long)-1;
     }
     else {
       buffer[i] = fd;
     }
 }
 set_fs(fs);
 return 0;
}
