#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/file.h>

#define N 10
#define SYS_OPEN 5
#define SYS_CLOSE 6
#define SYS_WRITE 4
#define SYS_OPEN_N 351

#define __SRD 0x0004 /* OK to read */
#define __SWR 0x0008 /* OK to write */
/* RD and WR are never simultaneously asserted */
#define __SRW 0x0010 /* open for reading & writing */

/*
* Return the (stdio) flags for a given mode.  Store the flags
* to be passed to an open() syscall through *optr.
* Return 0 on error.
*/
int
__sflags(mode, optr)
register char *mode;
int *optr;
{
register int ret, m, o;

switch (*mode++) {

case 'r': /* open for reading */
ret = __SRD;
m = O_RDONLY;
o = 0;
break;

case 'w': /* open for writing */
ret = __SWR;
m = O_WRONLY;
o = O_CREAT | O_TRUNC;
break;

case 'a': /* open for appending */
ret = __SWR;
m = O_WRONLY;
o = O_CREAT | O_APPEND;
break;

default: /* illegal mode */
errno = EINVAL;
return (0);
}

/* [rwa]\+ or [rwa]b\+ means read and write */
if (*mode == '+' || (*mode == 'b' && mode[1] == '+')) {
ret = __SRW;
m = O_RDWR;
}
*optr = m | o;
return (ret);
}

void testa_open() {
 /* De acordo com a man, os descritores de arquivos s��o n��o negativos */
 int  flags, oflags, fileDescriptor = -1;
 char filepath[15]   = "teste_open.txt";

 if ((flags = __sflags("w", &oflags)) == 0) {
   fprintf(stderr, "N��o abriu: %s. Erro na cria����o das flags.\n", filepath);
 }
 /* Chamada da syscall open */
 else if ((fileDescriptor = syscall(SYS_OPEN, filepath, oflags, DEFFILEMODE)) < 0) {
   fprintf(stderr, "N��o abriu: %s. Syscall n��o retornou descritor.\n", filepath);
 }
 else {
   printf("Abriu: %s com filedescriptor %d.\n", filepath, fileDescriptor);
   /* Chamada da syscall write */
   syscall(SYS_WRITE, fileDescriptor, filepath, 15);
   /* Chamada da syscall close */
   syscall(SYS_CLOSE, fileDescriptor);
 }
}

void testa_open_n() {
 int  i, flags, oflags;
 /* De acordo com a man, os descritores de arquivos s��o n��o negativos */
 int  fileDescriptors[N]  = {-1, -1, -1, -1, -1, -1, -1, -1,-1, -1};
 char filepaths[N][17]    = {"teste_open_0.txt", "teste_open_1.txt",
                             "teste_open_2.txt", "teste_open_3.txt",
                             "teste_open_4.txt", "teste_open_5.txt",
                             "teste_open_6.txt", "teste_open_7.txt",
                             "teste_open_8.txt", "teste_open_9.txt"};

 if ((flags = __sflags("w", &oflags)) == 0) {
   fprintf(stderr, "N��o abriu arquivos. Erro na cria����o das flags.\n");
 }
 /* Chamada da syscall open_n */
 else if (syscall(SYS_OPEN_N, filepaths, oflags, DEFFILEMODE, fileDescriptors, N, 17) != 0) {
   fprintf(stderr, "N��o abriu arquivos. Syscall n��o retornou descritores.\n");
 }
 else {
   printf("Abriu arquivos.\n");
   for (i = 0; i < N; i++) {
     printf("Arquivo %s tem descritor %d\n.", filepaths[i], fileDescriptors[i]);
     /* Chamada da syscall write */
     syscall(SYS_WRITE, fileDescriptors[i], filepaths[i], 17);
     /* Chamada da syscall close */
     syscall(SYS_CLOSE, fileDescriptors[i]);
   }
 }
}

int main() {

 testa_open();
 testa_open_n();
 return 0;
}
