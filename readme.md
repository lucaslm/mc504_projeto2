Implementação de syscall "open_n", que recebe um vetor de n strings com nomes de
arquivos pra abrir e coloca em um buffer passado de parâmetro os descritores desses arquivos.

Instalação:

    Alterar a tabela linux-3.12/arch/x86/syscalls/syscall_32.tlb, acrescentando uma nova linha ao final do arquivo, após a linha do finit_module

      350     i386    finit_module            sys_finit_module
      351     i386    open_n                  sys_open_n

    Incluir uma declaração da função sys_mycall em linux-3.12/include/linux/syscalls.h, novamente após a linha relacionada ao finit_module

    asmlinkage long sys_finit_module(int fd, const char _user *uargs, int flags);
    asmlinkage long sys_open_n(const char **filenames,
                               int flags,
                               umode_t mode,
                               int *buffer,
                               int n,
                               int filenames_length);

    Incluir o arquivo open_n.c no diretório linux-3.12/arch/x86/kernel/.

    Alterar o Makefile do diretório linux-3.12/arch/x86/kernel/, incluindo uma linha:

    obj-y                                   += open_n.o

    No diretório linux-3.12 executar

      $ make 
      

    Para não ter problemas com a quota, configure o diretório CCACHE utilizando o comando:

    $ export CCACHE_DIR="/tmp/.ccache"export CCACHE_DIR="/tmp/.ccache"

    Escrever um programa para testar a chamada em modo usuário. Pode ser o arquivo ex-open.c:

    Compilar o programa, considerando que a imagem foi feita para i386. As opções são:
        Compilar normalmente em uma máquina i386 (não é o caso das máquinas do laboratório).
        Utilizar o gcc cross compiler (não está configurado nas máquinas do laboratório).
        Utilizar o compilador http://www.onlinecompiler.net/ccplusplus. Note que não é seguro rodar um código gerado em um site como esses. No entanto, como utilizaremos este código apenas no ambiente virtualizado, não há problema (ou, pelo menos, o risco é bem menor). 
    Rodar o programa no QEMU:

    Devemos iniciar o sistema com o comando:

      qemu-system-x86_64 -hda mc504.img -kernel linux-3.12/arch/i386/boot/bzImage -append "ro root=/dev/hda" -hdb ex-open

    Após logar no sistema, devemos executar:

    $ cat /dev/hdb > ex-open
    $ chmod +x ex-open
    $ ./ex-open


